---
tags:
  - Content Health
  - Self-Service Measures
  - Integrating Feedback
  - Scenario 02
  - 'Technique 5.12: Self-Service Measures'
  - ❓
---

Status: Unknown ❓

##Mandatory##

Processing customer feedback on KCS articles. Ability to trigger workflow in order to process the feedback.  Respond, track and report on it.  Flag completed. Provide feedback of the value of giving feedback.  Same concept as Flag-it on article.  Should be done at any point where feedback is provided. (Ideally - to report on turn around time.
1.  Feedback can be on a separate object
2. Feedback can be tracked by state
3. What is the feedback, how long it there
4. What is the state, created date, date of change.

##Practice/Technique##

* Content Health
* Self-Service Measures
* Integrating Feedback

##Reference: KCS v6 Practices Guide##
Technique 5.12: Self-Service Measures


##Demo Requirements for Verified Only##
[Scenario 02](../scenario/02.md)

##OB/Config/Cust/NS/SF##


##Native Product Capability##

##Mechanism for Config##

##Config Strategy##

##Dependencies & Version Numbers##


##Documentation##
