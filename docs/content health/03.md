---
tags:
  - Content Health
  - The KCS Article State
  - Scenario 01
  - 'Technique 5.1: KCS Article Structure'
  - ❓
---

Status: Unknown ❓

##Mandatory##

The system shall support content being in a minimum of four article confidence levels:
Work In Progress
Not Validated
Validated
Archive



##Practice/Technique##

* Content Health
* The KCS Article State

##Reference: KCS v6 Practices Guide##

[Technique 5.1: KCS Article Structure](https://library.serviceinnovation.org/KCS/KCS_v6/KCS_v6_Practices_Guide/030/040/010/020)

##Demo Requirements for Verified Only##
[Scenario 01](../scenario/01.md)

##OB/Config/Cust/NS/SF##


##Native Product Capability##

##Mechanism for Config##

##Config Strategy##

##Dependencies & Version Numbers##


##Documentation##
