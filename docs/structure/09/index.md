---
tags:
  - Reuse
  - Linking
  - Referencing and Linking to Other Information Sources
  - Scenario 01
  - 'Technique 3.3: Linking'
  - ✅
---

Status: Complete ✅

##Mandatory##

KCS articles can have embedded hyperlinks to other sources of content, such as KCS articles, or content addressable by a URL.

##Practice/Technique##

* Reuse
* Linking
* Referencing and Linking to Other Information Sources

##Reference: KCS v6 Practices Guide##

[Technique 3.3: Linking](https://library.serviceinnovation.org/KCS/KCS_v6/KCS_v6_Practices_Guide/030/030/030/030)

##Demo Requirements for Verified Only##
[Scenario 01](../scenario/01.md)

##OB/Config/Cust/NS/SF##
###Out of the Box###
The Core modules 'editor' and 'ckeditor5' provide this ability. Editor allows
different WYSIWYG editors to be used. CKEditor is the default editor.

##Native Product Capability##

##Mechanism for Config##

##Config Strategy##

##Dependencies & Version Numbers##


##Documentation##
