---
tags:
  - Structure
  - Utilize Simple Templates
  - Scenario 07
  - 'Technique 2.1: Use a Simple Template'
  - ✅
  - drupal/core/node
---

Status: Complete ✅

##Mandatory##

Administrators may specify or adjust which fields are being present in any given
template and multiple templates may be maintained.

##Practice/Technique##

* Structure
* Utilize Simple Templates

##Reference: KCS v6 Practices Guide##

[Technique 2.1: Use a Simple Template](https://library.serviceinnovation.org/KCS/KCS_v6/KCS_v6_Practices_Guide/030/030/020/010)

##Demo Requirements for Verified Only##
[Scenario 07](/scenario/07.md)

##OB/Config/Cust/NS/SF##

###Out of the Box###
**admin/structure/types/manage/article/form-display**

![Node Form Display](./structure%2004%20node%20form%20display.png)

##Native Product Capability##
[Node](/tags/#drupalcorenode)

##Mechanism for Config##
A graphic interface is provided.

##Config Strategy##

The path and screen shot above shows the administration page for managing the
article's form. Fields can be rearranged, or hidden. Some fields have options to
render as different form elements.


##Dependencies & Version Numbers##


##Documentation##
