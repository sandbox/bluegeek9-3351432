---
tags:
  - Scenario 08
---

#Scenario 8: Reports#

| Scenario 8: Reports                                         | Comments | Complete |
|-------------------------------------------------------------|----------|----------|
|Show reporting capabilities. Must show and deliver copies of all the reports from the Self-Assessment worksheets (Performance Assessment Worksheet)